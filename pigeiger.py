#!/usr/bin/env python

import RPi.GPIO as GPIO
import threading
import requests
import time
import json
import datetime

def loadConfig():
	global LED_RED
	global LED_GREEN
	global GEIGER_PIN

	global UPLOAD_INTERVAL
	global FLASH_GREEN_ONTIME
	global FLASH_RED_PERIOD
	global ALARM_USV_H
	global TUBE_CONVERSION_RATIO
	global TUBE_DEADTIME
	global CPM_MEASUREMENT_PERIOD
	global UPLOAD_URL
	global UPLOAD_DATA
	
	try:
		with open('config.json') as f:
			config = json.load(f)

		# todo: error handling
			
		LED_RED = int(config["pins"]["led_red"], 10)
		LED_GREEN = int(config["pins"]["led_green"], 10)
		GEIGER_PIN = int(config["pins"]["tube"], 10)

		UPLOAD_INTERVAL = int(config["upload"]["upload_interval"], 10)
		UPLOAD_URL = config["upload"]["upload_url"]
		FLASH_GREEN_ONTIME = float(config["timing"]["flash_green_ontime"])
		FLASH_RED_PERIOD = float(config["timing"]["flash_red_period"])
		ALARM_USV_H = float(config["counting"]["alarm_microsievert"])
		TUBE_CONVERSION_RATIO = float(config["station_info"]["conversion_factor"])
		TUBE_DEADTIME = float(config["station_info"]["deadtime"])
		CPM_MEASUREMENT_PERIOD = int(config["counting"]["measurement_period"], 10)
		UPLOAD_DATA = dict()
		UPLOAD_DATA.update({'station_info' : config["station_info"]})
		UPLOAD_DATA.update({'apikey' : config["apikey"]})
	except:
		print("Error")
		
def ledFlash(countEvent):
	while (1):
		while flashAlarm:
			GPIO.output(LED_RED, GPIO.HIGH)
			time.sleep(FLASH_RED_PERIOD/2.0)
			GPIO.output(LED_RED, GPIO.LOW)
			time.sleep(FLASH_RED_PERIOD/2.0)
	
		# no timeout needed, as events should occur frequently
		countEvent.wait()
		countEvent.clear()
		GPIO.output(LED_GREEN, GPIO.HIGH)
		time.sleep(FLASH_GREEN_ONTIME)
		GPIO.output(LED_GREEN, GPIO.LOW)

def upload(uploadEvent):
	while (1):
		uploadEvent.wait()
		uploadEvent.clear()
		
		json_data = dict(UPLOAD_DATA)
		#todo: evt lokale kopie machen?
		json_data.update({'measurement' : {'raw_cpm' : str(int(rawCPM)), 'timestamp' : datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') } } )
		r = requests.post(UPLOAD_URL, json = json_data)
		print(r.text)
		if (r.text == "OK"):
			print("Upload success")
		else:
			print("Upload failed")
		#r.close()
		
	# todo: write proper upload manager with support for reuploads + signed data packets
		
def tick(channel):
	global currentCounts

	countEvent.set()
	currentCounts = currentCounts + 1

def main():
	global currentCounts
	global rawCPM
	global countEvent
	global uploadEvent
	global flashAlarm

	# Load JSON Settings
	loadConfig()

	currentCounts = 0
	rawCPM = 0
	flashAlarm = False
		
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(LED_GREEN, GPIO.OUT)
	GPIO.setup(LED_RED, GPIO.OUT)
	GPIO.setup(GEIGER_PIN, GPIO.IN)

	countEvent = threading.Event()
	tLED = threading.Thread(name='non-block', target=ledFlash, args=(countEvent,))
	tLED.start()
	
	uploadEvent = threading.Event()
	tUpload = threading.Thread(name='non-block', target=upload, args=(uploadEvent,))
	tUpload.start()
	
	
	GPIO.add_event_detect(GEIGER_PIN,GPIO.FALLING)
	GPIO.add_event_callback(GEIGER_PIN,tick)
	
	counts_per_second = [0] * CPM_MEASUREMENT_PERIOD
	cps_index = 0
	currentCounts = 0
	seconds_running = 0
	
	while (1): # endless loop for CPM updates
		#todo: blocking for other threads
		counts_per_second[cps_index] = currentCounts
		currentCounts = 0
		cps_index = (cps_index + 1) % CPM_MEASUREMENT_PERIOD
		seconds_running = seconds_running + 1
		
		tmp = 0
		for i in range(CPM_MEASUREMENT_PERIOD):
			tmp = tmp + counts_per_second[i]
		if seconds_running < CPM_MEASUREMENT_PERIOD:
			tmp = tmp * CPM_MEASUREMENT_PERIOD / seconds_running

		rawCPM = tmp
		
		# calculate display CPM (as deadtime etc. is also taken care of in backend)
		displayCPM = tmp/(1-tmp*TUBE_DEADTIME)
		print("Current CPM:	  " + str(displayCPM))

		displayuSv = displayCPM * TUBE_CONVERSION_RATIO
		print("Current uSv/h: " + str(displayuSv))

		flashAlarm = (displayuSv >= ALARM_USV_H)

		if (seconds_running % UPLOAD_INTERVAL) == 0:
			uploadEvent.set()

		time.sleep(1.0)

	GPIO.cleanup()

if __name__== "__main__":
	#todo: keyboard interrupt geht wegen threading nicht
	try:
		main()
	except KeyboardInterrupt:
		GPIO.cleanup()
