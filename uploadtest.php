<?php
date_default_timezone_set('Europe/Berlin');

if ($_SERVER['REQUEST_METHOD'] === 'POST') 
{
	$filename = date('Y-m-d_H_i_s') . ".json";
	$data = file_get_contents("php://input");
	 
	if (file_put_contents($filename, $data) > 0)
	{
		echo "OK";
	}
	else
	{
		echo "failed";
	}
}
else
{
	?>
	<html>
		<head>
			<title>Last Logs</title>
		</head>
		<body>
			<a href="." >Click here for directory listing!</a>
		</body>
	</html>
	<?php
}
?>