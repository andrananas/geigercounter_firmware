<?php
require 'config.php';

// First verify that the data is complete and correct
if ($_SERVER['REQUEST_METHOD'] !== 'POST')
{
	die("OOPS, seems like you got the wrong page :)");
}

$data = file_get_contents("php://input");
$json = json_decode($data);

if ($json === NULL)
{
	die("invalid request");
}

$pdo = new PDO($DB_TYPE . ":host=" . $DB_HOST . ";dbname=" . $DB_NAME, $DB_USER, $DB_PASS);

if (!isset($json->apikey))
{
	die("api key needed");
}

$select_apikey = $pdo->prepare("SELECT * FROM apikeys WHERE apikey=:apikey");
$select_apikey->execute(array('apikey' => strtolower($json->apikey))); 

if ($apikey = $select_apikey->fetch())
{
	if ($apikey['active'] == 0)
	{
		die("api key has been deactivated");
	}
	
	$apikey_id = $apikey['id'];
}
else
{
	die("invalid api key");
}

// from this point, the user is authorized

// significant stuff that can change and require a new station entry:
// - name
// - tube_name (new tube)
// - longitude, latitude (new place)
// - conversion_factor (new conversion factor)
// - deadtime (new deadtime)

// find last entry for this api key
$select_station = $pdo->prepare("SELECT * FROM stations WHERE apikey_id=:apikey_id ORDER BY id DESC LIMIT 1");
$select_station->execute(array('apikey_id' => $apikey_id));

//TODO:
// - add a new station if parameters change
// - always calculate usv/h if conversion rate is set
// - if only cpm is set but no conversion ratio save cpm
// - "active" column for tubes per apikey

if ($station = $select_station->fetch())
{
	// this api_key already has a valid station
	// check if station data has changed
	// to track which measurements have been made with which equipment, a new station is inserted if the data has changed
	$id = $station['id'];

	if ((isset($json->station_info->name) AND ($station['station_name'] != $json->station_info->name)) OR
		(isset($json->station_info->tube_name) AND ($station['tube_name'] != $json->station_info->tube_name)) OR 
		(isset($json->station_info->longitude) AND ($station['longitude'] != $json->station_info->longitude)) OR
		(isset($json->station_info->latitude) AND ($station['latitude'] != $json->station_info->latitude)) OR
		(isset($json->station_info->conversion_factor) AND ($station['conversion_factor'] != $json->station_info->conversion_factor)) OR
		(isset($json->station_info->deadtime) AND ($station['deadtime'] != $json->station_info->deadtime)) OR 
		(isset($json->station_info->first_use) AND ($station['first_use'] != $json->station_info->first_use)))
	{		
		// need to insert a new station
		$new_station = array(	'apikey_id' => $apikey_id,
								'station_name' => (isset($json->station_info->name) ? $json->station_info->name : $station['station_name']),
								'tube_name' => (isset($json->station_info->tube_name) ? $json->station_info->tube_name : $station['tube_name']),
								'longitude' => (isset($json->station_info->longitude) ? $json->station_info->longitude : $station['longitude']),
								'latitude' => (isset($json->station_info->latitude) ? $json->station_info->latitude : $station['latitude']),
								'conversion_factor' => (isset($json->station_info->conversion_factor) ? $json->station_info->conversion_factor : $station['conversion_factor']),
								'deadtime' => (isset($json->station_info->deadtime) ? $json->station_info->deadtime : $station['deadtime']),
								'first_use' => (isset($json->station_info->first_use) ? $json->station_info->first_use : $station['first_use']));
		
		$insert_station = $pdo->prepare("INSERT INTO stations SET apikey_id=:apikey_id, station_name=:station_name, tube_name=:tube_name, longitude=:longitude, latitude=:latitude, conversion_factor=:conversion_factor, deadtime=:deadtime, first_use=:first_use");
		$insert_station->execute($new_station);
		$new_station['id'] = $pdo->lastInsertId();
		
		$station = $new_station;
	}
}
else
{
	// generate API key and insert station into db
	die("currently only registered stations allowed :(");
}

if (isset($json->measurement))
{
	// raw cpm is set, calculate deadtime compensated CPM
	if (isset($json->measurement->raw_cpm))
	{
		$raw_cpm = floatval($json->measurement->raw_cpm);
		
		$deadtime = floatval($station['deadtime']);
		if ($deadtime > 0)
		{
			$cpm = $raw_cpm/(1-$raw_cpm*$deadtime);
		}
		else
		{
			$cpm = $raw_cpm;
		}
	}
	else if (isset($json->measurement->cpm))
	{
		// cpm is set, we assume deadtime is already compensated. set raw_cpm to 0 as it is not supplied
		$raw_cpm = -1;
		$cpm = floatval($json->measurement->cpm);
	}
	else
	{
		// check if at least usv/h value is supplied
		$raw_cpm = -1;
		$cpm = -1;
	}
	
	// cpm and raw_cpm are set, now calculate usv/h
	$conversion_factor = floatval($station['conversion_factor']);
	if (($conversion_factor > 0) AND ($cpm > 0))
	{
		$ush = $conversion_factor * $cpm;
	}
	else if (isset($json->measurement->usv))
	{
		$ush = floatval($json->measurement->usv);
	}
	else
	{
		$ush = -1;
	}
	
	if (($raw_cpm < 0) AND ($cpm < 0) AND ($ush < 0))
	{
		// nothing to insert, exit
		die("incomplete measurement data");
	}
	
	// calculate timestamp
	$timestamp = strtotime($json->measurement->timestamp);
	if ($timestamp === FALSE)
	{
		$timestamp = time();
	}
	
	// insert measurement
	$insert_measurement = $pdo->prepare("INSERT INTO measurements SET station_id=:station_id, raw_cpm=:raw_cpm, cpm=:cpm, ush=:ush, timestamp=:timestamp");
	$insert_measurement->execute(array('station_id' => $id, 'raw_cpm' => $raw_cpm, 'cpm' => $cpm, 'ush' => $ush, 'timestamp' => date("Y-m-d H:i:s",$timestamp)));
	echo "OK";
}


?>