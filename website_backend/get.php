<?php
require 'config.php';

if (!isset($_GET['apikey_id']))
{
	die("please supply apikey_id");
}

$pdo = new PDO($DB_TYPE . ":host=" . $DB_HOST . ";dbname=" . $DB_NAME, $DB_USER, $DB_PASS);

$lasthour = time() - 60 * 60;

$select_statement = $pdo->prepare("SELECT ush FROM measurements WHERE (timestamp >= :timestamp) AND (station_id = ANY (SELECT id FROM stations WHERE apikey_id=:apikey_id))");
$select_statement->execute(array('timestamp' => date("Y-m-d H:i:s",$lasthour), 'apikey_id' => $_GET['apikey_id']));

$numrows = 0;
$sum = 0;
while($row = $select_statement->fetch())
{
	$sum += $row['ush'];
	$numrows++;
}

if ($numrows == 0)
	echo "no recent data";
else
	echo ($sum/$numrows);
?>